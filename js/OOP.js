class Person
{
    construct(fn, ln, gen)
    {
        this.firstName = fn;
        this.lastName = ln;
        this.gender = gen;
    }

    static introduction()
    {
        return 'Cao ja sam ' + this.firstName;
    }
}

Person.staticVar = "blahhh";
var kristijan = new Person('kristijan', 'koncar', 19);

class Programmer extends Person
{
    construct (job)
    {
        this.job = job;
    }
}

