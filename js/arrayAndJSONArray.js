//Array

var a1 = ["a", "b", "c"]; 
var a2 = new Array("d", "e", "f");
var deca = a2[0] + a2[1] + a2[2] + a1[0];

//JSON, JSON array

var pet = '{"name":"dzeki", "type":"ker"}';

var pets = '{"pets":[{"name":"stojan", "type":"osa"}, {"name":"reks", "type":"gibanicar"}, {"name":"zlatan", "type":"ribica"}]}';
var arr = JSON.parse(pets);