ar promiseCount = 0;

function testPromise()
{
    var thisPromiseCount = ++promiseCount;

    var p1 = new Promise(
        function(resolve, reject)
        {
            alert("Promise " + thisPromiseCount + " started");

            window.setTimeout(
                function() {
                    resolve(thisPromiseCount);
                }, Math.random() * 2000 + 1000);
        }
    );

    p1.then(
        function(val) {
            alert("Promise " + val + " fulfilled");
        })
    .catch(
        function(reason) {
            alert("Handle rejected promise " + reason + " here.");
        });

    alert("Promise " + thisPromiseCount + " made");
}